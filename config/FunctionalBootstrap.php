<?php

declare(strict_types=1);

require_once $_ENV['PWD'] . '/.Build/vendor/autoload.php';

include $_ENV['PWD'] . '/.Build/vendor/typo3/testing-framework/Resources/Core/Build/FunctionalTestsBootstrap.php';

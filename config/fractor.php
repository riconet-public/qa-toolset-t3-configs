<?php

use a9f\Fractor\Configuration\FractorConfiguration;
use a9f\Typo3Fractor\Set\Typo3LevelSetList;

$dir = $_ENV['PWD'];

return FractorConfiguration::configure()
    ->withPaths([$dir])
    ->withSets([
        Typo3LevelSetList::UP_TO_TYPO3_13
    ]);
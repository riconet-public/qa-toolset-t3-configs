<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Php71\Rector\FuncCall\CountOnNullRector;
use Rector\Set\ValueObject\LevelSetList;
use Ssch\TYPO3Rector\Configuration\Typo3Option;
use Ssch\TYPO3Rector\Rector\General\ConvertImplicitVariablesToExplicitGlobalsRector;
use Ssch\TYPO3Rector\Rector\General\ExtEmConfRector;
use Ssch\TYPO3Rector\Set\Typo3LevelSetList;
use Ssch\TYPO3Rector\Set\Typo3SetList;

return static function (RectorConfig $rectorConfig): void {
    $dir = $_ENV['PWD'];

    $rectorConfig->sets([
        Typo3LevelSetList::UP_TO_TYPO3_12,
        LevelSetList::UP_TO_PHP_81,
        Typo3SetList::TCA_120,
        Typo3SetList::TCA_123,
        Typo3SetList::REGISTER_ICONS_TO_ICON,
    ]);
    $rectorConfig->rule(ConvertImplicitVariablesToExplicitGlobalsRector::class);
    $rectorConfig->ruleWithConfiguration(ExtEmConfRector::class, [
        ExtEmConfRector::ADDITIONAL_VALUES_TO_BE_REMOVED => []
    ]);
    $rectorConfig->phpstanConfig(Typo3Option::PHPSTAN_FOR_RECTOR_PATH);
    $rectorConfig->importNames();
    $rectorConfig->disableParallel();
    $rectorConfig->importShortClasses(false);
    $rectorConfig->phpVersion(PhpVersion::PHP_81);
    $rectorConfig->skip([
        CountOnNullRector::class,
        $dir . '/**/Configuration/ExtensionBuilder/*',
        $dir . '/Resources/*',
        $dir . '/vendor/*',
        $dir . '/node_modules/*',
        $dir . '/Build/*',
        $dir . '/public/*',
        $dir . '/.github/*',
        $dir . '/.Build/*',
        $dir . '/.idea/*',
    ]);
};

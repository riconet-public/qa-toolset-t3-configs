<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Ssch\TYPO3Rector\Configuration\Typo3Option;
use Ssch\TYPO3Rector\Set\Typo3LevelSetList;
use Ssch\TYPO3Rector\Set\Typo3SetList;

return static function (RectorConfig $rectorConfig): void {
    $dir = $_ENV['PWD'];

    $rectorConfig->sets([
        Typo3LevelSetList::UP_TO_TYPO3_13,
        LevelSetList::UP_TO_PHP_82,
        Typo3SetList::CODE_QUALITY,
        Typo3SetList::GENERAL,
        Typo3SetList::TYPO3_13,
    ]);
    $rectorConfig->phpstanConfig(Typo3Option::PHPSTAN_FOR_RECTOR_PATH);
    $rectorConfig->importNames();
    $rectorConfig->disableParallel();
    $rectorConfig->importShortClasses(false);
    $rectorConfig->skip([
        $dir . '/**/Configuration/ExtensionBuilder/*',
        $dir . '/Resources/*',
        $dir . '/vendor/*',
        $dir . '/node_modules/*',
        $dir . '/Build/*',
        $dir . '/public/*',
        $dir . '/.github/*',
        $dir . '/.Build/*',
        $dir . '/.idea/*',
    ]);
};
